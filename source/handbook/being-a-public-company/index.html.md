---
layout: markdown_page
title: "Being a public company"
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Being a public company

1. As detailed in our [company strategy](/company/strategy/), GitLab wants to go public in CY2020, specifically on Wednesday, November 18, 2020.
1. Going public is like graduating from high school, a great day but it shouldn't be the biggest thing you achieve in life.
1. We have a [strategy with clear goals for November 18, 2023](/company/strategy/#sequence) such as getting to $500m ARR.
1. About stock prices, the father of value investing, Benjamin Graham, [explained this concept](https://news.morningstar.com/classroom2/course.asp?docId=142901&page=7) by saying that: "In the short run, the market is like a voting machine, tallying up which firms are popular and unpopular. But in the long run, the market is like a weighing machine, assessing the substance of a company." The message is: What matters in the long run is a company's actual underlying business performance so we should focus on our [KPIs](/handbook/ceo/kpis/) and particularly on growing our [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv).
1. When you bring up the stock price in a work conversation the response should be: lets not get distracted by the stock price we can't control in the short term and focus on growing IACV instead.
1. We can't control our stock price nor the volatility of that price in the short-term, of the two the volatility compared to the rest of the market is something we can reduce by realizing our plans, meeting expectations, and making sure investors understand our financial model, priorities, expectations, forecast and strategy.
1. We will make everyone an insider, this means your trading is restricted but we can share financial information inside the company.
1. We never want to go back in transparency. For example we never released financial information publicly because as a public company you can only do that when it is audited.
1. We should evaluate spending proposals with Return On Investment (ROI) calculations, industry benchmarks, and the most efficient way to do things. Never compare the amount spend to our company valuation, revenue, or assets.

This statement is being made pursuant to, and in accordance with, Rule 135 under the Securities Act of 1933, as amended (the “Securities Act”) and shall not constitute an offer to sell, or the solicitation of an offer to buy, any securities. Any offers, solicitations or offers to buy, or any sales of securities will be made in accordance with the registration requirements of the Securities Act.